<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Trouble Shooting of Tool Archives with CoVeriTeam-Remote

This document provides instructions to use CoVeriTeam to execute
 the tools participating in the
 software-verification competition and software-testing competition,
 on the same infrastructure on which the competition is run.
More detailed documentation on CoVeriTeam can be found [on the main page](../README.md).
Please refer to the detailed documentation in case of need for clarification.

## Steps to follow to test a verifier or a tester
1. Clone the [CoVeriTeam repository](https://gitlab.com/sosy-lab/software/coveriteam):
   `git clone https://gitlab.com/sosy-lab/software/coveriteam.git`
2. Go to folder `examples` inside the the cloned repository.
   The `examples` folder contains two scripts: [`remote_examples_verifier.sh`](../examples/remote_examples_verifier.sh) and [`remote_examples_tester.sh`](../examples/remote_examples_tester.sh).
   These scripts contain a sample command for each of the participating tools in the competitions.
3. Please copy the command for your tool and execute it.

Here is an example command for the verifier `CPAchecker`:
```bash
../bin/coveriteam verifier-C.cvt \
  --input verifier_path=../actors/cpa-seq.yml \
  --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
  --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
  --input data_model=ILP32 \
  --remote
```

This command will execute the participating tool `cpa-seq` (define in actor definition `actors/cpa-seq.yml`)
in the version `default`
on the program `c/ldv-regression/test02.c`
for the property `c/properties/unreach-call.prp` using the service (`--remote` flag).

There are 4 different programs to execute the verifiers:
1. `verifier-C.cvt` to execute the first available archive in the corresponding actor definition for a verifier for C programs
2. `verifier-C-specific-version.cvt` to execute the archive specified by the given version (`--input verifier_verion=<VERSION>`) in the corresponding actor definition for a verifier for C programs
3. `verifier-Java.cvt` to execute the first available archive in the corresponding actor definition for a verifier for Java programs
4. `verifier-Java-specific-version.cvt` to execute the archive specified by the given version (`--input verifier_verion=<VERSION>`) in the corresponding actor definition for a verifier for Java programs

Please also read the section on restrictions below.
More information can be found in the [documentation](index.md).

### Witness validation
We also support validating witnesses produced by a verifier by a witness validator.
The program used for this purpose is: `validating-verifier.cvt`.
Here is an example command that runs the validator `witness-linter` after the verifier `uautomizer`:
```bash
../bin/coveriteam validating-verifier.cvt \
  --input verifier_path=../actors/uautomizer.yml \
  --input verifier_version=default \
  --input validator_path=../actors/witness-linter.yml \
  --input validator_version=default \
  --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
  --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
  --input data_model=ILP32 \
  --remote
```

The command connects to a service that in turn tries to execute the tool on the competition infrastructure.
We have imposed some restrictions on what can be executed.
The commands complying with these restriction should be executed by the service,
otherwise the user would get an error message from the service.

### Remote Locations

There are two endpoints that can be called when using the `--remote` flag:

- https://coveriteam-service.sosy-lab.org/execute
- https://coveriteam.sosy-lab.org/execute

the **default** is https://coveriteam-service.sosy-lab.org/execute .
If you wish to point to another URL this can be done with: 
```
--remote-url <URL>
``` 

**What is the difference between the two endpoints?**

With https://coveriteam-service.sosy-lab.org/execute we offer a sandbox installation of CoVeriTeam on an external server. 
The main difference here is that you **do not** need to add your tool trusted repositories before using the service. 
We aim to match the installed packages there as closely as possible to the SV-COMP machines. 
This endpoint also allows you to upload files that are not part of the sv-benchmarks.

We encourage you to integrate this service in your CI!
However, this endpoint is still in its early phase and has limited resources. 
It is designed to quickly test your tool and provide quick access to your tool during tutorial sessions and workshops.

If you really wish to check if your already uploaded tool works on the SV-COMP 
machines, e.g., there is a serious bug that you fail to reproduce elsewhere, then you 
can still invoke the "old" service by adding 
`--remote-url https://coveriteam.sosy-lab.org/execute` 
as an argument to your CoVeriTeam call.

If you want to use the endpoint https://coveriteam.sosy-lab.org/execute **please read** the information on trusted repositories and restrictions for competition hardware below!


### Trusted tool repositories
CoVeriTeam service downloads and executes tools available at the trusted location.

Trusted locations are:
1. Tool archives repositories for competitions for SVCOMP (https://gitlab.com/sosy-lab/sv-comp/archives-<YYYY>),
  and TESTCOMP (https://gitlab.com/sosy-lab/test-comp/archives-<YYYY>)
2. Tool specific trusted repository in the project group: https://gitlab.com/sosy-lab/benchmarking/coveriteam-archives.

Process for requesting a tool specific repository:
1. Please contact [Sudeep Kanav](https://www.sosy-lab.org/people/kanav/), or [Philipp Wendler](https://www.sosy-lab.org/people/wendler/) to create a repository in the project group: https://gitlab.com/sosy-lab/benchmarking/coveriteam-archives
2. Please push your tool archive to the `main` branch of the repository (create the branch if not present)
3. Update the [actor definition]((../actors)) for your tool to include a version pointing to this repository.
   (Actor definitions support multiple versions. As an example look at the [actor definition of PRTest](../actors/prtest.yml#L11).)
4. Create a merge request to merge the actor definition.

Once this merge request has been merged you should be able to use the CoVeriTeam service to execute 
the archive of your tool available on the tool specific truested repository on the competition infrastructure.

## Restrictions for execution on the competition infrastructure (vcloud instance):
1. The actor definition (yml file) must be in the main branch of the [CoVeriTeam repository](https://gitlab.com/sosy-lab/software/coveriteam)
2. The program and property files must be in the main branch of the [sv-benchmarks repository](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks)
3. Only those tool info modules can be used which are either released with the benchexec, or are in the main branch of the [benchexec repository](https://github.com/sosy-lab/benchexec).
   A user can use a url to specify a tool info module in the actor definition YAML file, instead of the name of the tool info module.
4. Only those tools are supported which are in sv competition's repository.

5. Only the following `cvt` files are allowed to be executed: 
`verifier-C-specific-version.cvt`,
`verifier-C.cvt`,
`verifier-Java-specific-version.cvt`,
`verifier-Java.cvt`,
`tester.cvt`,
`tester-specific-version.cvt`,
and `validating-verifier.cvt` from the CoVeriTeam's repository.



We do not allow user to provide files. Only the file names are considered and the files are downloaded from the repository.
But still, the file paths should exist on your machine, these will be given to the service.
But the publicly accessible instance of the service clips these paths and downloads files from the respective repositories. 
 
## FAQs
1. Why don't I just see the output produced by my tool on `stdout`? \
   You can use the option `--verbose` for this. 
   The output you see is produced by `CoVeriTeam` for the executed program (which might execute more than one tools).
   With `-verbose` we print the output logs from the tools.
2. My tool was executed with different options than the ones defined in benchmark definitions. Why?\
   `CoVeriTeam` reads options from the [actor definition files](../actors) that
    were created from benchmark definition files but might be outdated. 
    You can either create a merge request to update the actor definition, 
    or report it in the issue tracker of verifier archives or tester archives.
3. Executing the tool using `CoVeriTeam service` behaves differently than the official pre-runs. Why?\
   We know of one scenario where this could happen.
   The service would transport the complete tool folder to the cloud,
   whereas running it with `benchexec` would transport only the required paths 
   set using the variable `REQUIRED_PATHS` in the tool info module.
   This might result in a successful execution using the service, but not with `benchexec`.
   

// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// Verifier-Based Tester

w2test = ActorFactory.create(WitnessToTest, "../actors/cpachecker-witness-to-test.yml", "witness2test");
ver = ActorFactory.create(ProgramVerifier, "../actors/cpachecker.yml", "svcomp22");
tester = SEQUENCE(ver,w2test);

// Print type information about the composition (for illustration)
print("\nFollowing is the type of the actor tester:");
print(tester);

// Prepare test inputs.
prog = ArtifactFactory.create(CProgram, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
ip = {'program':prog, 'spec':spec};

// Execute the actor on the inputs.
res = execute(tester, ip);
print("The following artifacts were produced by the execution:");
print(res);

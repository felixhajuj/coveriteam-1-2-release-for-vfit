// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// A CoVeriTeam program to execut a verifier.

program = ArtifactFactory.create(CProgram, program_path, data_model);
specification = ArtifactFactory.create(BehaviorSpecification, specification_path);
inputs = {'program':program, 'spec':specification};

// Select an appropriate verifier backend.
verifier_selector = ActorFactory.create(AlgorithmSelector, "../actors/algo-selector-cst-transform.yml");
selected_verifier_name = execute(verifier_selector, inputs);

// Second stage: use the verifier backend.
verifier_def = selected_verifier_name.actordef;
selected_verifier = ActorFactory.create(ProgramVerifier, verifier_def);

// Execute the selected verifier on the inputs.
res = execute(selected_verifier, inputs);
print("The following artifacts were produced by the execution:");
print(res);

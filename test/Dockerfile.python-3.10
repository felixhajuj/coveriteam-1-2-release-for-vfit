# This file is part of CondTest,
# a collection of example implementations for conditional testing:
# https://gitlab.com/sosy-lab/software/conditional-testing
#
# SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# This is a Docker image for running the tests.
# It should be pushed to registry.gitlab.com/sosy-lab/software/test-format/test
# and will be used by CI as declared in .gitlab-ci.yml.
#
# Commands for updating the image:
# docker build -t registry.gitlab.com/sosy-lab/software/coveriteam/test:python-3.8 - < test/Dockerfile.python-3.8
# docker push registry.gitlab.com/sosy-lab/software/coveriteam/test

FROM python:3.10-bullseye

RUN apt update && apt upgrade -y && apt install -y \
  lxcfs \
  sudo \
  python3-pip \
  gcc \
  gcc-multilib \
  clang \
  clang-9 \
  openjdk-11-jre-headless \
  libopenmpi-dev \
  openmpi-bin 

RUN pip install -U pip && pip install \
  'pytest' \
  'numpy' \
  'pytest-cov' \
  'coverage' \
  'psutil' \
  'antlr4-python3-runtime' \
  'requests' \
  'mpi4py' \
  'requests-ratelimiter' \
  'tqdm' \
  'coloredlogs' \
  'benchexec==3.17'
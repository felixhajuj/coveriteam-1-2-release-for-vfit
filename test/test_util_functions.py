# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
import os
import shutil
from pathlib import Path

import pytest
from test_util import enabled_download

from coveriteam.language.actorconfig import ActorConfig
from coveriteam.language.artifact import Artifact, CProgram, TestSuite, Verdict
from coveriteam.util import subsumes, unzip

pmp = pytest.mark.parametrize


def test_get_type_per_key():
    pass


def test_get_type_per_key_dict_list():
    pass


@pmp(
    ["left", "right", "expected"],
    [
        ({"verdict": Artifact, "program": CProgram}, {"verdict": Verdict}, True),
        ({"verdict": Artifact, "program": CProgram}, {"verdict": Artifact}, True),
        ({"verdict": Artifact, "program": CProgram}, {"verdict": TestSuite}, False),
        (
            {"verdict": Artifact, "program": CProgram},
            {"verdict": Verdict, "test_suite": TestSuite},
            False,
        ),
    ],
)
def test_subsumes(left, right, expected):
    print(subsumes(left, right) == expected)


def test_archive_structure_support(capsys):
    # Create dummy test file not fulfilling structure expectations
    test_dir = Path("test/test_archives")
    if os.path.exists(test_dir):
        shutil.rmtree(test_dir)

    target_dir = test_dir / "output"
    root_dir = test_dir / "input"
    out_dir = test_dir / "zipped"

    sub1 = root_dir / "sub"
    sub2 = root_dir / "sub2"

    for directory in [test_dir, root_dir, out_dir]:
        os.makedirs(directory, exist_ok=True)

    def create_txt(path):
        with open(path, "w") as file:
            file.write("file content")

    def create_zip(name):
        return shutil.make_archive(out_dir / name, "zip", root_dir)

    # Case single file zip
    create_txt(root_dir / "test_file1.txt")
    zip1 = create_zip("archive1")

    # Case expected archive structure
    os.makedirs(sub1, exist_ok=True)
    create_txt(sub1 / "test_file2.txt")
    zip2 = create_zip("archive2")

    # Case multiple top level directories
    os.makedirs(sub2, exist_ok=True)
    create_txt(sub2 / "test_file3.txt")
    zip3 = create_zip("archive3")

    with capsys.disabled():
        # Should fail, len(root_dir) < 1
        with pytest.raises(ValueError):  # noqa pt011
            unzip(Path(zip1), target_dir)
            print("ValueError was triggered as expected.")

        # Should fail, len(root_dir) >= 2
        with pytest.raises(ValueError):  # noqa pt011
            unzip(Path(zip3), target_dir)
            print("ValueError was triggered as expected.")

        # Should succeed, len(root_dir) == 1
        unzip(Path(zip2), target_dir)
        assert True, print("Archive structure was accepted as excepted.")

        # Should succeed, len(root_dir) < 1
        with enabled_download():
            ActorConfig("actors/uautomizer.yml", "default")
            assert True, print("Archive structure was accepted as excepted.")

    # Clean up
    shutil.rmtree(test_dir)

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from io import StringIO
from pathlib import Path

import pytest
import yaml
from antlr4 import CommonTokenStream, InputStream, ParseTreeWalker

from coveriteam.coveriteam import CoVeriTeam
from coveriteam.interpreter.utility_visitors import (
    FileRename,
    RenameSchemaValidationError,
    validate_rename_data,
)
from coveriteam.parser.CoVeriLangLexer import CoVeriLangLexer
from coveriteam.parser.CoVeriLangListener import CoVeriLangListener
from coveriteam.parser.CoVeriLangParser import CoVeriLangParser


def test_simple_renaming():
    class TestListener(CoVeriLangListener):
        def enterAtomic(self, ctx: CoVeriLangParser.AtomicContext):
            assert ctx.STRING()[0].getText() == '"changed.yml"'
            assert ctx.STRING()[1].getText() == '"2.2"'
            assert ctx.STRING()[2].getText() == '"res.yml"'

    cvt = """
a = ActorFactory.create(ProgramVerifier, "some.yml", "2.2", "res.yml");
exec(a, { })
    """.strip()

    stream = InputStream(cvt)
    lexer = CoVeriLangLexer(stream)
    token_stream = CommonTokenStream(lexer)
    parser = CoVeriLangParser(token_stream)
    tree = parser.program()

    sub = {"some.yml": "changed.yml"}

    FileRename("test.cvt", sub).visit(tree)

    listener = TestListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)


def test_no_applying_renaming():
    class TestListener(CoVeriLangListener):
        def enterAtomic(self, ctx: CoVeriLangParser.AtomicContext):
            assert ctx.STRING()[0].getText() == '"some.yml"'
            assert ctx.STRING()[1].getText() == '"2.2"'
            assert ctx.STRING()[2].getText() == '"res.yml"'

    cvt = """
a = ActorFactory.create(ProgramVerifier, "some.yml", "2.2", "res.yml");
exec(a, { })
    """.strip()

    stream = InputStream(cvt)
    lexer = CoVeriLangLexer(stream)
    token_stream = CommonTokenStream(lexer)
    parser = CoVeriLangParser(token_stream)
    tree = parser.program()

    sub = {"some1.yml": "changed.yml"}

    FileRename("test.cvt", sub).visit(tree)

    listener = TestListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)


def test_multiple_renaming():
    class TestListener(CoVeriLangListener):
        def enterAtomic(self, ctx: CoVeriLangParser.AtomicContext):
            if not ctx.STRING():
                return
            assert ctx.STRING()[0].getText() == '"changed.yml"'
            assert ctx.STRING()[1].getText() == '"2.2"'
            assert ctx.STRING()[2].getText() == '"res.yml"'

        def enterSpec_stmt(self, ctx: CoVeriLangParser.Spec_stmtContext):
            a = ctx.assignable()
            if a.STRING():
                assert a.STRING().getText() == '"changed_as_well.yml"'

        def enterArtifact(self, ctx: CoVeriLangParser.ArtifactContext):
            assert ctx.STRING().getText() == '"golden_monkey.spc"'

    cvt = """
a = ActorFactory.create(ProgramVerifier, "some.yml", "2.2", "res.yml");
actor2 = "indiana_jones.yml";
i = ActorFactory.create(ProgramTester, actor2);
artifact = ArtifactFactory.create(BehaviorSpecification, "forbidden.spc");
exec(a, { });
    """.strip()

    stream = InputStream(cvt)
    lexer = CoVeriLangLexer(stream)
    token_stream = CommonTokenStream(lexer)
    parser = CoVeriLangParser(token_stream)
    tree = parser.program()

    sub = {
        "some.yml": "changed.yml",
        "indiana_jones.yml": "changed_as_well.yml",
        "forbidden.spc": "golden_monkey.spc",
    }

    FileRename("test.cvt", sub).visit(tree)

    listener = TestListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)


def test_rename_validation_good():
    rename_file = StringIO(
        """
some.yml: changed.yml
indinana_jones.yml: changed_as_well.yml
forbidden.spc: golden_monkey.spc
    """.strip()
    )

    data = yaml.safe_load(rename_file)

    data2 = validate_rename_data(data)

    assert data2 is data


def test_rename_validation_bad():
    rename_file = StringIO(
        """
some.yml:
    - changed.yml
indinana_jones.yml: changed_as_well.yml
forbidden.spc: golden_monkey.spc
    """.strip()
    )

    data = yaml.safe_load(rename_file)

    with pytest.raises(RenameSchemaValidationError):
        validate_rename_data(data)


def test_rename_validation_with_json():
    rename_file = StringIO(
        """
        {
            "some.yml": "changed.yml",
            "indinana_jones.yml": "changed_as_well.yml",
            "forbidden.spc": "golden_monkey.spc"
        }
        """
    )

    data = yaml.safe_load(rename_file)
    validate_rename_data(data)


EXAMPLES = Path(__file__).parent.parent / "examples"


@pytest.fixture()
def setup_cache():
    return ["--cache-dir", str(Path(__file__).parent.parent / "cache")]


def in_examples(path: str) -> Path:
    return str(EXAMPLES / path)


def argument(key, path) -> str:
    is_path = {"actordef_dir"}
    if "path" in key or key in is_path:
        return f"{key}={in_examples(path)}"
    return f"{key}={path}"


def test_renaming_e2e(setup_cache, capsys):
    inputs = [in_examples("test-data/rename/broken.cvt")]
    inputs += ["--rename-from", in_examples("test-data/rename/renaming.yml")]
    inputs += ["--input", argument("program_path", "test-data/c/error.i")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += ["--input", argument("version", "default")]
    inputs += setup_cache

    CoVeriTeam().start(inputs)

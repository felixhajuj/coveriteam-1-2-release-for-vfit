# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

antlr4-python3-runtime>=4.8
benchexec>=2.6
requests>=2.24
coloredlogs>=15.0.1
tqdm>=4.64.0
docker>=docker-6.0.1

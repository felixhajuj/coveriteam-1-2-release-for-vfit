# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from coveriteam.language.actor import Transformer, Verifier, Validator
from coveriteam.language.atomicactor import AtomicActor, TaskExecutionInformation
from coveriteam.language.artifact import (
    Program,
    Specification,
    Witness,
    Verdict,
    TestSuite,
    TestSpecification,
    Predicates,
    AtomicActorDefinition,
    Report,
    Artifact,
)
from coveriteam.util import create_archive
from coveriteam.utils.extractors import (
    extract_verdict,
    extract_witness,
    extract_predicates,
    extract_artifact,
)
from benchexec.result import get_result_classification
import os
from typing import Dict


class ProgramVerifier(Verifier, AtomicActor):
    _input_artifacts = {"program": Program, "spec": Specification}
    _output_artifacts = {"verdict": Verdict, "witness": Witness}
    _result_files_patterns = ["**/*.graphml"]

    # It is a deliberate decision to not have the init function. We do not want anyone to
    # create instances of this class.

    def _prepare_args(self, program, spec):
        return [program.path, spec.path]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        witness = extract_witness(te_info, "**/*.graphml")
        verdict = extract_verdict(self.tool._tool, te_info)
        verdict_class = get_result_classification(verdict.verdict)
        if not witness.path and verdict_class in ("true", "false"):
            verdict = Verdict("VERDICT_DISCARDED(NO WITNESS)")

        return {
            "verdict": verdict,
            "witness": witness,
        }


class ProgramVerifierWithoutWitness(Verifier, AtomicActor):
    _input_artifacts = {"program": Program, "spec": Specification}
    _output_artifacts = {"verdict": Verdict}
    _result_files_patterns = []

    # It is a deliberate decision to not have the init function. We do not want anyone to
    # create instances of this class.

    def _prepare_args(self, program, spec):
        return [program.path, spec.path]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        return {"verdict": extract_verdict(self.tool._tool, te_info)}


class ProgramVerifierWithHTMLReport(Verifier, AtomicActor):
    _input_artifacts = {"program": Program, "spec": Specification}
    _output_artifacts = {"verdict": Verdict, "witness": Witness, "report": Report}
    _result_files_patterns = ["**", "**/*.html"]

    # It is a deliberate decision to not have the init function. We do not want anyone to
    # create instances of this class.

    def _prepare_args(self, program, spec):
        return [program.path, spec.path]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        witness = extract_witness(te_info, "**/*.graphml")
        verdict = extract_verdict(self.tool._tool, te_info)
        report_path = extract_artifact(te_info, "**/*.html")
        report = Report(report_path)
        verdict_class = get_result_classification(verdict.verdict)
        if not witness.path and verdict_class in ("true", "false"):
            verdict = Verdict("VERDICT_DISCARDED(NO WITNESS)")

        return {"verdict": verdict, "witness": witness, "report": report}


class CoVeriTeamBasedProgramVerifier(ProgramVerifier):
    _input_artifacts = {
        "program": Program,
        "spec": Specification,
        "verifier_config_to_use": AtomicActorDefinition,
    }

    def _prepare_args(self, program, spec, verifier_config_to_use):
        return [program.path, spec.path]

    def _get_arg_substitutions(self, program, spec, verifier_config_to_use):
        return {"verifier_config_to_use": str(verifier_config_to_use)}

    def _extract_last_witness(self, te_info):
        artifact_list = list(te_info.log_dir.glob("**/*.graphml"))
        if not artifact_list:
            artifact_list = [""]
        return Witness(artifact_list[-1])

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        return {
            "verdict": extract_verdict(self.tool._tool, te_info),
            "witness": self._extract_last_witness(te_info),
        }


class PredicateBasedProgramVerifier(ProgramVerifier):
    _input_artifacts = {
        "program": Program,
        "spec": Specification,
        "predicates": Predicates,
    }
    _output_artifacts = {
        "verdict": Verdict,
        "witness": Witness,
        "predicates": Predicates,
    }
    _result_files_patterns = ["**/*.graphml", "**/predmap.txt"]

    # It is a deliberate decision to not have the init function. We do not want anyone to
    # create instances of this class.
    def _get_arg_substitutions(self, program, spec, predicates):
        return {"predicates": self.tool.get_relative_path_to_tool(predicates.path)}

    def _prepare_args(self, program, spec, predicates):
        return [program.path, spec.path]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        return {
            "verdict": extract_verdict(self.tool._tool, te_info),
            "witness": extract_witness(te_info, "**/*.graphml"),
            "predicates": extract_predicates(te_info, "**/predmap.txt"),
        }


class ProgramValidator(Validator, AtomicActor):
    _input_artifacts = {
        "program": Program,
        "spec": Specification,
        "witness": Witness,
        "verdict": Verdict,
    }
    _output_artifacts = {"verdict": Verdict, "witness": Witness}
    _result_files_patterns = ["**/*.graphml"]

    def _get_arg_substitutions(self, program, spec, witness, verdict):
        return {"witness": self.tool.get_relative_path_to_tool(witness.path)}

    def _prepare_args(self, program, spec, witness, verdict):
        return [program.path, spec.path]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        return {
            "verdict": extract_verdict(self.tool._tool, te_info),
            "witness": extract_witness(te_info, "**/*.graphml"),
        }


class PredicateBasedProgramValidator(ProgramValidator):
    _input_artifacts = {
        "program": Program,
        "spec": Specification,
        "witness": Witness,
        "verdict": Verdict,
        "predicates": Predicates,
    }
    _output_artifacts = {
        "verdict": Verdict,
        "witness": Witness,
        "predicates": Predicates,
    }
    _result_files_patterns = ["**/*.graphml", "**/predmap.txt"]

    # It is a deliberate decision to not have the init function. We do not want anyone to
    # create instances of this class.

    def _get_arg_substitutions(self, program, spec, witness, verdict, predicates):
        return {
            "witness": self.tool.get_relative_path_to_tool(witness.path),
            "predicates": self.tool.get_relative_path_to_tool(predicates.path),
        }

    def _prepare_args(self, program, spec, witness, verdict, predicates):
        return [program.path, spec.path]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        return {
            "verdict": extract_verdict(self.tool._tool, te_info),
            "witness": extract_witness(te_info, "**/*.graphml"),
            "predicates": extract_predicates(te_info, "**/predmap.txt"),
        }


class TestValidator(Transformer, AtomicActor):
    _input_artifacts = {
        "program": Program,
        "test_suite": TestSuite,
        "test_spec": TestSpecification,
    }
    _output_artifacts = {"verdict": Verdict}
    _result_files_patterns = []

    def _prepare_args(self, program, test_suite, test_spec):
        options_spec = ["--goal", self.tool.get_relative_path_to_tool(test_spec.path)]
        testzip = os.path.join(os.path.dirname(test_suite.path), "test_suite.zip")
        create_archive(test_suite.path, testzip)
        testzip = self.tool.get_relative_path_to_tool(testzip)
        options_test_suite = ["--test-suite", testzip]
        options = options_test_suite + options_spec
        return [program.path, "", options]

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        return {"verdict": extract_verdict(self.tool._tool, te_info)}

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.11.1
from antlr4 import *

if __name__ is not None and "." in __name__:
    from .CoVeriLangParser import CoVeriLangParser
else:
    from CoVeriLangParser import CoVeriLangParser


# This class defines a complete listener for a parse tree produced by CoVeriLangParser.
class CoVeriLangListener(ParseTreeListener):
    # Enter a parse tree produced by CoVeriLangParser#program.
    def enterProgram(self, ctx: CoVeriLangParser.ProgramContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#program.
    def exitProgram(self, ctx: CoVeriLangParser.ProgramContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#fun_decl.
    def enterFun_decl(self, ctx: CoVeriLangParser.Fun_declContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#fun_decl.
    def exitFun_decl(self, ctx: CoVeriLangParser.Fun_declContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#stmt_block.
    def enterStmt_block(self, ctx: CoVeriLangParser.Stmt_blockContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#stmt_block.
    def exitStmt_block(self, ctx: CoVeriLangParser.Stmt_blockContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#id_list.
    def enterId_list(self, ctx: CoVeriLangParser.Id_listContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#id_list.
    def exitId_list(self, ctx: CoVeriLangParser.Id_listContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#stmt.
    def enterStmt(self, ctx: CoVeriLangParser.StmtContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#stmt.
    def exitStmt(self, ctx: CoVeriLangParser.StmtContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#spec_stmt.
    def enterSpec_stmt(self, ctx: CoVeriLangParser.Spec_stmtContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#spec_stmt.
    def exitSpec_stmt(self, ctx: CoVeriLangParser.Spec_stmtContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#PrintActor.
    def enterPrintActor(self, ctx: CoVeriLangParser.PrintActorContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#PrintActor.
    def exitPrintActor(self, ctx: CoVeriLangParser.PrintActorContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ExecuteActor.
    def enterExecuteActor(self, ctx: CoVeriLangParser.ExecuteActorContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ExecuteActor.
    def exitExecuteActor(self, ctx: CoVeriLangParser.ExecuteActorContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#return_stmt.
    def enterReturn_stmt(self, ctx: CoVeriLangParser.Return_stmtContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#return_stmt.
    def exitReturn_stmt(self, ctx: CoVeriLangParser.Return_stmtContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#SetActorName.
    def enterSetActorName(self, ctx: CoVeriLangParser.SetActorNameContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#SetActorName.
    def exitSetActorName(self, ctx: CoVeriLangParser.SetActorNameContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#arg_map.
    def enterArg_map(self, ctx: CoVeriLangParser.Arg_mapContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#arg_map.
    def exitArg_map(self, ctx: CoVeriLangParser.Arg_mapContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#map_item_list.
    def enterMap_item_list(self, ctx: CoVeriLangParser.Map_item_listContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#map_item_list.
    def exitMap_item_list(self, ctx: CoVeriLangParser.Map_item_listContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#map_item.
    def enterMap_item(self, ctx: CoVeriLangParser.Map_itemContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#map_item.
    def exitMap_item(self, ctx: CoVeriLangParser.Map_itemContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#assignable.
    def enterAssignable(self, ctx: CoVeriLangParser.AssignableContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#assignable.
    def exitAssignable(self, ctx: CoVeriLangParser.AssignableContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Atomic.
    def enterAtomic(self, ctx: CoVeriLangParser.AtomicContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Atomic.
    def exitAtomic(self, ctx: CoVeriLangParser.AtomicContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#FunCall.
    def enterFunCall(self, ctx: CoVeriLangParser.FunCallContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#FunCall.
    def exitFunCall(self, ctx: CoVeriLangParser.FunCallContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Utility.
    def enterUtility(self, ctx: CoVeriLangParser.UtilityContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Utility.
    def exitUtility(self, ctx: CoVeriLangParser.UtilityContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Sequence.
    def enterSequence(self, ctx: CoVeriLangParser.SequenceContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Sequence.
    def exitSequence(self, ctx: CoVeriLangParser.SequenceContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ITE.
    def enterITE(self, ctx: CoVeriLangParser.ITEContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ITE.
    def exitITE(self, ctx: CoVeriLangParser.ITEContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Iterative.
    def enterIterative(self, ctx: CoVeriLangParser.IterativeContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Iterative.
    def exitIterative(self, ctx: CoVeriLangParser.IterativeContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Parallel.
    def enterParallel(self, ctx: CoVeriLangParser.ParallelContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Parallel.
    def exitParallel(self, ctx: CoVeriLangParser.ParallelContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ParallelPortfolio.
    def enterParallelPortfolio(self, ctx: CoVeriLangParser.ParallelPortfolioContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ParallelPortfolio.
    def exitParallelPortfolio(self, ctx: CoVeriLangParser.ParallelPortfolioContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ActorAlias.
    def enterActorAlias(self, ctx: CoVeriLangParser.ActorAliasContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ActorAlias.
    def exitActorAlias(self, ctx: CoVeriLangParser.ActorAliasContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Parenthesis.
    def enterParenthesis(self, ctx: CoVeriLangParser.ParenthesisContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Parenthesis.
    def exitParenthesis(self, ctx: CoVeriLangParser.ParenthesisContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Joiner.
    def enterJoiner(self, ctx: CoVeriLangParser.JoinerContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Joiner.
    def exitJoiner(self, ctx: CoVeriLangParser.JoinerContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Setter.
    def enterSetter(self, ctx: CoVeriLangParser.SetterContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Setter.
    def exitSetter(self, ctx: CoVeriLangParser.SetterContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Comparator.
    def enterComparator(self, ctx: CoVeriLangParser.ComparatorContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Comparator.
    def exitComparator(self, ctx: CoVeriLangParser.ComparatorContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Copy.
    def enterCopy(self, ctx: CoVeriLangParser.CopyContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Copy.
    def exitCopy(self, ctx: CoVeriLangParser.CopyContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Rename.
    def enterRename(self, ctx: CoVeriLangParser.RenameContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Rename.
    def exitRename(self, ctx: CoVeriLangParser.RenameContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#TestSpecToSpec.
    def enterTestSpecToSpec(self, ctx: CoVeriLangParser.TestSpecToSpecContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#TestSpecToSpec.
    def exitTestSpecToSpec(self, ctx: CoVeriLangParser.TestSpecToSpecContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#SpecToTestSpec.
    def enterSpecToTestSpec(self, ctx: CoVeriLangParser.SpecToTestSpecContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#SpecToTestSpec.
    def exitSpecToTestSpec(self, ctx: CoVeriLangParser.SpecToTestSpecContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ClassificationToActorDefinition.
    def enterClassificationToActorDefinition(
        self, ctx: CoVeriLangParser.ClassificationToActorDefinitionContext
    ):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ClassificationToActorDefinition.
    def exitClassificationToActorDefinition(
        self, ctx: CoVeriLangParser.ClassificationToActorDefinitionContext
    ):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Identity.
    def enterIdentity(self, ctx: CoVeriLangParser.IdentityContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Identity.
    def exitIdentity(self, ctx: CoVeriLangParser.IdentityContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#CreateArtifact.
    def enterCreateArtifact(self, ctx: CoVeriLangParser.CreateArtifactContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#CreateArtifact.
    def exitCreateArtifact(self, ctx: CoVeriLangParser.CreateArtifactContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ArtifactAlias.
    def enterArtifactAlias(self, ctx: CoVeriLangParser.ArtifactAliasContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ArtifactAlias.
    def exitArtifactAlias(self, ctx: CoVeriLangParser.ArtifactAliasContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ArtifactFromMapItem.
    def enterArtifactFromMapItem(
        self, ctx: CoVeriLangParser.ArtifactFromMapItemContext
    ):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ArtifactFromMapItem.
    def exitArtifactFromMapItem(self, ctx: CoVeriLangParser.ArtifactFromMapItemContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#artifact_type.
    def enterArtifact_type(self, ctx: CoVeriLangParser.Artifact_typeContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#artifact_type.
    def exitArtifact_type(self, ctx: CoVeriLangParser.Artifact_typeContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#data_model.
    def enterData_model(self, ctx: CoVeriLangParser.Data_modelContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#data_model.
    def exitData_model(self, ctx: CoVeriLangParser.Data_modelContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#actor_type.
    def enterActor_type(self, ctx: CoVeriLangParser.Actor_typeContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#actor_type.
    def exitActor_type(self, ctx: CoVeriLangParser.Actor_typeContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ExpAlias.
    def enterExpAlias(self, ctx: CoVeriLangParser.ExpAliasContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ExpAlias.
    def exitExpAlias(self, ctx: CoVeriLangParser.ExpAliasContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#NotLogical.
    def enterNotLogical(self, ctx: CoVeriLangParser.NotLogicalContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#NotLogical.
    def exitNotLogical(self, ctx: CoVeriLangParser.NotLogicalContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#InstanceOf.
    def enterInstanceOf(self, ctx: CoVeriLangParser.InstanceOfContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#InstanceOf.
    def exitInstanceOf(self, ctx: CoVeriLangParser.InstanceOfContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#ElementOf.
    def enterElementOf(self, ctx: CoVeriLangParser.ElementOfContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#ElementOf.
    def exitElementOf(self, ctx: CoVeriLangParser.ElementOfContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#QuotedExpAliasForArtifacts.
    def enterQuotedExpAliasForArtifacts(
        self, ctx: CoVeriLangParser.QuotedExpAliasForArtifactsContext
    ):
        pass

    # Exit a parse tree produced by CoVeriLangParser#QuotedExpAliasForArtifacts.
    def exitQuotedExpAliasForArtifacts(
        self, ctx: CoVeriLangParser.QuotedExpAliasForArtifactsContext
    ):
        pass

    # Enter a parse tree produced by CoVeriLangParser#BinaryLogical.
    def enterBinaryLogical(self, ctx: CoVeriLangParser.BinaryLogicalContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#BinaryLogical.
    def exitBinaryLogical(self, ctx: CoVeriLangParser.BinaryLogicalContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#Paren.
    def enterParen(self, ctx: CoVeriLangParser.ParenContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#Paren.
    def exitParen(self, ctx: CoVeriLangParser.ParenContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#verdict_list.
    def enterVerdict_list(self, ctx: CoVeriLangParser.Verdict_listContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#verdict_list.
    def exitVerdict_list(self, ctx: CoVeriLangParser.Verdict_listContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#tc_exp.
    def enterTc_exp(self, ctx: CoVeriLangParser.Tc_expContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#tc_exp.
    def exitTc_exp(self, ctx: CoVeriLangParser.Tc_expContext):
        pass

    # Enter a parse tree produced by CoVeriLangParser#quoted_ID_with_maybe_type.
    def enterQuoted_ID_with_maybe_type(
        self, ctx: CoVeriLangParser.Quoted_ID_with_maybe_typeContext
    ):
        pass

    # Exit a parse tree produced by CoVeriLangParser#quoted_ID_with_maybe_type.
    def exitQuoted_ID_with_maybe_type(
        self, ctx: CoVeriLangParser.Quoted_ID_with_maybe_typeContext
    ):
        pass

    # Enter a parse tree produced by CoVeriLangParser#quoted_ID.
    def enterQuoted_ID(self, ctx: CoVeriLangParser.Quoted_IDContext):
        pass

    # Exit a parse tree produced by CoVeriLangParser#quoted_ID.
    def exitQuoted_ID(self, ctx: CoVeriLangParser.Quoted_IDContext):
        pass


del CoVeriLangParser

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from coveriteam.language import CoVeriLangException
from coveriteam.language.artifact import Verdict, Witness, Predicates
from pathlib import Path
from typing import Optional


def extract_verdict(tool_info_instance, te_info):
    return Verdict(tool_info_instance.determine_result(te_info.run_object))


def extract_witness(te_info, pattern):
    path = extract_artifact(te_info, pattern)
    if path is None:
        return Witness("")
    return Witness(path)


def extract_predicates(te_info, pattern):
    path = extract_artifact(te_info, pattern)
    if path is None:
        return Predicates("")
    return Predicates(path)


def extract_artifact(te_info, pattern: str) -> Optional[Path]:
    artifact_list = list(te_info.log_dir.glob(pattern))

    if not artifact_list:
        return None

    if len(artifact_list) != 1:
        msg = (
            "Found more than one artifact matching pattern. Following are the artifacts found: \n"
            + "\n".join(map(str, artifact_list))
        )
        raise CoVeriLangException(msg)

    return artifact_list[0]

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import json
import logging
import os
import re
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Dict, List, Union

import benchexec
import yaml

import coveriteam.util as util
from coveriteam.language import CoVeriLangException
from coveriteam.util import (
    CVT_DEBUG_LEVEL,
    SESSION_PROVIDER,
    download,
    download_if_needed,
    get_ARCHIVE_DOWNLOAD_PATH,
    is_url,
    unzip,
)


class ActorDefinitionLoader(yaml.SafeLoader):
    def __init__(self, stream):
        self._root = Path(stream.name).parent
        super(ActorDefinitionLoader, self).__init__(stream)

    def include(self, node):
        filename = self._root / self.construct_scalar(node)
        with filename.open("r") as f:
            d = yaml.load(f, ActorDefinitionLoader)  # noqa S506
            return dict_merge(d, {"imported_file": filename})

    @staticmethod
    def load_config(path):
        with open(path, "r") as f:
            try:
                d = yaml.load(f, ActorDefinitionLoader)  # noqa S506
            except yaml.YAMLError as e:
                msg = "Actor config yaml file {} is invalid: {}".format(path, e)
                raise CoVeriLangException(msg, 203)

            return d

    @staticmethod
    def resolve_includes(d):
        # Check if "imports" exist
        imports = d.pop("imports", None)
        if not imports:
            return d

        if not isinstance(imports, list):
            imports = [imports]

        di = {}
        for i in imports:
            i.pop("imported_file", None)
            di = dict_merge(di, ActorDefinitionLoader.resolve_includes(i))
        return dict_merge(di, d)

    @staticmethod
    def collect_included_files(d):
        f = d.pop("imported_file", None)
        f = [] if f is None else [str(f)]
        # Check if "imports" exist
        imports = d.pop("imports", None)
        if not imports:
            return f

        if not isinstance(imports, list):
            imports = [imports]

        fs = []
        for i in imports:
            fs += ActorDefinitionLoader.collect_included_files(i)
        return fs + f


class FormatDelegate(ABC):
    """
    The delegate extracts the relevant options from actor configs given as yaml.
    These options are:
        - reslim
        - version
        - options
        - archive_location
        - archive_name
        - tool_dir
        - environment
        - toolinfo_module
        - actor_name
    """

    def __init__(self):
        self.CONFIG_VERSIONS = None
        self.archive_location = None
        self.archive_name = None
        self.environment = None
        self.tool_dir = None

    @abstractmethod
    def get_archive_location(self) -> str:
        pass

    @abstractmethod
    def get_tool_dir(self) -> Path:
        pass

    @abstractmethod
    def get_environment(self) -> Dict[str, str]:
        pass

    @abstractmethod
    def get_toolinfo_module(self) -> str:
        pass

    @abstractmethod
    def get_version(self) -> str:
        pass

    @abstractmethod
    def get_options(self) -> List[str]:
        pass

    @abstractmethod
    def get_resource_limits(self) -> Dict[str, Union[int, str]]:
        pass

    @abstractmethod
    def get_actor_name(self) -> str:
        pass

    @abstractmethod
    def get_cache_path(self) -> Path:
        pass

    @staticmethod
    def _sanitize_resource_limits(resource_limits):
        if not resource_limits:
            return None
        if resource_limits.get("memlimit"):
            resource_limits["memlimit"] = benchexec.util.parse_memory_value(
                resource_limits.get("memlimit")
            )
        if resource_limits.get("timelimit"):
            resource_limits["timelimit"] = benchexec.util.parse_timespan_value(
                resource_limits.get("timelimit")
            )
        return resource_limits

    def find_version_from_given(self, version):
        if not version:
            check_missing_version_exception(
                name=self.get_actor_name(), versions=self.CONFIG_VERSIONS
            )
            return str(self.CONFIG_VERSIONS[0]["version"])

        return str(version)

    def find_tool_config_from_given(self, versions):
        tool_configs = [x for x in versions if str(x["version"]) == self.get_version()]

        if len(tool_configs) < 1:
            raise CoVeriLangException(
                f"Version {self.get_version()} not found for actor {self.get_actor_name()}"
            )
        if len(tool_configs) > 1:
            raise CoVeriLangException(
                "There a multiple versions in the yaml file with the same name!", 2
            )
        tool_config = tool_configs[0]

        if tool_config is None:
            raise CoVeriLangException(
                f'{self.get_actor_name()} doesn\'t recognize the requested version "{self.get_version()}"!'
            )
        return tool_config

    def handle_tool_source_types(self, tool_config):
        has_url, has_doi = check_tool_sources(self, tool_config)

        if has_doi:
            doi = tool_config["doi"]
            check_allowed_doi(doi)
            if util.UPDATE_CACHE:
                self.archive_location, checksum = get_archive_url_from_zenodo_doi(doi)
                update_cache_entry(
                    self.get_cache_path(), "checksum_precached", checksum
                )
                self.archive_name = self.get_archive_name()
            self.tool_dir = str(util.get_INSTALL_DIR() / doi.replace("/", "-"))

        if has_url:
            # TODO add own instance field for abstract version diff of "location" and "url"
            if isinstance(self, FormatDelegateV1_2):
                self.archive_location = tool_config["location"]
            else:
                self.archive_location = tool_config["url"]
            check_policy_compliance(self)
            self.archive_name = self.get_archive_name()
            # Keeping this path as str instead of Path because it is going to be used with string paths mostly.
            self.tool_dir = self.get_tool_installation_dir()

        if util.UPDATE_CACHE:
            update_cache_entry(self.get_cache_path(), "url", self.archive_location)
        self.environment = tool_config.get("environment", None)

        if self.environment and not hasattr(self, "archive_location"):
            dir_name = f"{self.get_actor_name()}-{sanitize_url_string(self.environment['image_repo'])}-{self.environment['image_tag']}"
            self.tool_dir = str(util.get_INSTALL_DIR() / dir_name)
            tool_dir_path = Path(self.tool_dir)
            if not tool_dir_path.is_dir():
                tool_dir_path.mkdir(parents=True)

    def get_tool_installation_dir(self):
        # I think it is easier for debugging if the dir name starts with the archive name.
        # So, we take out the archive name and put it in the front instead of at the end.
        url_root = self.archive_location.rpartition("/")[0]
        if self.archive_name.endswith(".zip"):
            archive_name = self.archive_name.rpartition(".")[0]
        tool_dir_name = archive_name + "-" + sanitize_url_string(url_root)
        return str(util.get_INSTALL_DIR() / tool_dir_name)

    def init_archive_cache(self):
        if not self.get_cache_path().is_file():
            with open(self.get_cache_path(), "w") as json_cache:
                json.dump(
                    {"url": "", "path": "", "etag": "", "checksum": ""}, json_cache
                )

    def get_archive_name(self):
        archive_name = self.archive_location.rpartition("/")[-1]

        if archive_name.endswith(".zip"):
            archive_name = archive_name.rpartition(".")[0]
        archive_name += ".zip"
        return archive_name


class MissingKeysException(Exception):
    pass


# TODO move abstract FormatDelegate and FormatDelegateVX_Y to own .py-files for better maintainability
class FormatDelegateV1_2(FormatDelegate):
    def __init__(self, config: Dict[str, Any], version=None, resource_limits=None):
        super().__init__()
        self._config = config
        self.CONFIG_VERSIONS = self._config["archives"]
        self.__check_actor_definition_integrity()
        self.__sanitize_yaml_dict()

        if resource_limits:
            raise CoVeriLangException(
                "Resource limits via extra yaml files in the CoVeriTeam Actor are currently only supported when using"
                " format version 2.0. To use resource limits, please upgrade the format or "
                " use the `resourcelimits` tag directly in the actor definition.",
                200,
            )

        self.actor_name = self._config["actor_name"] + (
            f"-{version}" if version else ""
        )
        self.cache_path = util.get_CACHE_DIR_PATH() / Path(
            self.actor_name + "_cache.json"
        )
        self.init_archive_cache()

        self.reslim = self._config["resourcelimits"]

        # We take the first available version in case of None or an empty string.
        self.version = self.find_version_from_given(version)

        tool_config = self.find_tool_config_from_given(self.CONFIG_VERSIONS)

        if "options" in tool_config:
            self.options = tool_config["options"]
        else:
            logging.debug("No options override found, hence using global options.")
            self.options = self._config["options"]

        if "resourcelimits" in tool_config:
            self.reslim = tool_config["resourcelimits"]

        self.handle_tool_source_types(tool_config)

    def __check_actor_definition_integrity(self):
        # check if the essential tags are present.
        # Essentiality of tags can be defined in a schema.
        essential_tags = {
            "toolinfo_module",
            "resourcelimits",
            "actor_name",
            "archives",
            "format_version",
        }
        diff = essential_tags - self._config.keys()
        if diff:
            raise MissingKeysException(diff)

    def __sanitize_yaml_dict(self):
        reslim = self._sanitize_resource_limits(
            self._config.get("resourcelimits", None)
        )
        if reslim:
            self._config["resourcelimits"] = reslim

        # archives can not be empty after __check_actor_definition_integrity
        for version in self.CONFIG_VERSIONS:
            reslim = self._sanitize_resource_limits(version.get("resourcelimits", None))
            if reslim:
                version["resourcelimits"] = reslim

    # implement abstract methods
    def get_archive_location(self):
        return self.archive_location

    def get_cache_path(self):
        return self.cache_path

    def get_tool_dir(self):
        return self.tool_dir

    def get_environment(self):
        return self.environment

    def get_toolinfo_module(self):
        return self._config["toolinfo_module"]

    def get_version(self):
        return self.version

    def get_options(self):
        return self.options

    def get_resource_limits(self):
        return self.reslim

    def get_actor_name(self):
        return self.actor_name


class FormatDelegateV2_0(FormatDelegate):
    def __init__(
        self,
        config: Dict[str, Any],
        version: str = None,
        resource_limits: Dict[str, Union[int, str]] = None,
    ):
        super().__init__()
        self._config = FormatDelegateV2_0.__sanitize_yaml_dict(config)
        self.CONFIG_VERSIONS = self._config["versions"]
        self.__check_actor_definition_integrity()

        self.actor_name = sanitize_url_string(self._config["name"]).replace(
            " ", "_"
        ) + (f"-{version}" if version else "")
        self.cache_path = util.get_CACHE_DIR_PATH() / Path(
            self.actor_name + "_cache.json"
        )
        self.init_archive_cache()
        self.reslim = self._config.get("resourcelimits", None)

        self.version = self.find_version_from_given(version)

        tool_config = self.find_tool_config_from_given(self.CONFIG_VERSIONS)

        if "benchexec_toolinfo_options" in tool_config:
            self.options = tool_config["benchexec_toolinfo_options"]
        else:
            self.options = []

        if "resourcelimits" in tool_config:
            self.reslim = tool_config["resourcelimits"]

        if not (resource_limits or self.reslim):
            resource_limits = {
                "memlimit": "8 GB",
                "timelimit": "2 min",
                "cpuCore": "2",
            }
            logging.warning(
                "No resources given for actor %s. Using default values: %s.",
                self.actor_name,
                resource_limits,
            )
        if self.reslim is None:
            self.reslim = FormatDelegateV2_0._sanitize_resource_limits(resource_limits)
            logging.log(
                CVT_DEBUG_LEVEL,
                "Using externally provided resource limits: %s.",
                self.reslim,
            )

        self.handle_tool_source_types(tool_config)

    # implement abstract methods
    def get_archive_location(self):
        return self.archive_location

    def get_cache_path(self):
        return self.cache_path

    def get_tool_dir(self):
        return self.tool_dir

    def get_environment(self):
        return self.environment

    def get_toolinfo_module(self):
        return self._config["benchexec_toolinfo_module"]

    def get_version(self):
        return self.version

    def get_options(self):
        return self.options

    def get_resource_limits(self):
        return self.reslim

    def get_actor_name(self):
        return self.actor_name

    @staticmethod
    def __sanitize_yaml_dict(config: Dict[str, Any]):
        reslim = FormatDelegateV2_0._sanitize_resource_limits(
            config.get("resourcelimits", None)
        )
        if reslim:
            config["resourcelimits"] = reslim

        for archive in config.get("versions", []):
            reslim = FormatDelegateV2_0._sanitize_resource_limits(
                archive.get("resourcelimits", None)
            )
            if reslim:
                archive["resourcelimits"] = reslim

        return config

    # TODO move to FormateDelegate, using essential tags as delegate version dependant property
    def __check_actor_definition_integrity(self):
        # check if the essential tags are present.
        # Essentiality of tags can be defined in a schema.
        essential_tags = {
            "benchexec_toolinfo_module",
            "name",
            "versions",
        }
        diff = essential_tags - self._config.keys()
        if diff:
            raise MissingKeysException(diff)


class ActorConfig:
    @property
    def actor_name(self):
        return self.delegate.get_actor_name()

    @property
    def reslim(self):
        return self.delegate.get_resource_limits()

    @property
    def version(self):
        return self.delegate.get_version()

    @property
    def options(self):
        return self.delegate.get_options()

    @property
    def archive_location(self):
        return self.delegate.get_archive_location()

    @property
    def cache_path(self):
        return self.delegate.get_cache_path()

    @property
    def archive_name(self):
        return self.delegate.get_archive_name()

    @property
    def tool_dir(self):
        return self.delegate.get_tool_dir()

    @property
    def environment(self):
        return self.delegate.get_environment()

    def __init__(self, path, version=None, resource_limits: Path = None):
        ActorDefinitionLoader.add_constructor("!include", ActorDefinitionLoader.include)
        self.path = path
        config = self.read_yaml(path)

        logging.log(CVT_DEBUG_LEVEL, "External resource limits: %s.", resource_limits)
        if resource_limits:
            resource_limits = self.read_yaml(resource_limits)

        format_version = config.get("format_version", None)
        format_version = config.get("fmtools_format_version", format_version)

        if format_version is None:
            raise CoVeriLangException(
                "The actor config file does not specify a format version."
            )
        try:
            if format_version == "1.2":
                self.delegate: FormatDelegate = FormatDelegateV1_2(
                    config, version, resource_limits
                )
            elif format_version == "2.0":
                self.delegate: FormatDelegate = FormatDelegateV2_0(
                    config, version, resource_limits
                )
            else:
                raise CoVeriLangException(f"Unknown format version: {format_version}.")
        except MissingKeysException as e:
            msg = (
                "The following tags are missing in the actor config YAML: "
                + self.path
                + "\n"
                + "\n".join(e.args[0])
            )
            raise CoVeriLangException(msg, 200)

        if not version:
            log_version_warnings(self.delegate.get_actor_name())

        if util.UPDATE_CACHE and self.delegate.get_archive_location():
            self.__install_if_needed()

        self.tool_name = self.__resolve_tool_info_module()

    @staticmethod
    def read_yaml(path: Path):
        config = ActorDefinitionLoader.load_config(path)
        return ActorDefinitionLoader.resolve_includes(config)

    def __install_if_needed(self):
        target_dir = Path(self.delegate.get_tool_dir())

        self.check_valid_version_name_format()

        archive_download_path = (
            get_ARCHIVE_DOWNLOAD_PATH() / self.delegate.get_actor_name()
        )
        update_cache_entry(
            self.delegate.get_cache_path(), "path", str(archive_download_path.resolve())
        )
        print(f"Download check for actor {self.delegate.get_actor_name()}")
        downloaded = download_if_needed(self.delegate.get_cache_path())

        # Unzip only if downloaded or the tool directory does not exist.
        if downloaded or not target_dir.is_dir():
            print("Installing the actor: " + self.delegate.get_actor_name() + "......")
            unzip(archive_download_path, target_dir)

    def __resolve_tool_info_module(self):
        """
        1. Check if it is a URL.
        2. If a URL then download it and save it to the TI cache.
        3. Infer the module name and return it.
        """
        # TODO the extraction of the tool info module name should be separated from downloading.
        ti = self.delegate.get_toolinfo_module()
        if is_url(ti):
            filename = util.get_TOOL_INFO_DOWNLOAD_PATH() / ti.rpartition("/")[2]
            if util.UPDATE_CACHE:
                download(ti, filename)
            ti = "." + filename.name

        if ti.endswith(".py"):
            ti = ti.rpartition(".")[0]

        return ti

    def check_valid_version_name_format(self):
        if not re.compile("^[A-Za-z0-9-._]+$").match(self.archive_name):
            raise CoVeriLangException(
                "Can't download tool because the version name contains invalid characters!",
                200,
            )


def dict_merge(d1, d2):
    # This function recursively merges (updates) the dictionaries.
    for k in d2.keys():
        if k in d1.keys():
            if isinstance(d1[k], dict) and isinstance(d2[k], dict):
                d1[k] = dict_merge(d1[k], d2[k])
            elif isinstance(d1[k], dict) or isinstance(d2[k], dict):
                # TODO this could be and XOR
                # We raise an error when one of the values is a dict, but not the other.
                msg = "YAML file could not be parsed. Clash in the tag: %r" % k
                raise CoVeriLangException(msg, 201)
            d1[k] = d2[k]
        else:
            d1[k] = d2[k]

    return d1


def sanitize_url_string(url: str):
    """
    This function sanitizes the url string by replacing the special characters with a hyphen.
    """
    return url.replace("://", "-").replace("/", "-").replace(":", "-").replace("~", "-")


def check_tool_sources(delegate, tool_config):
    has_doi = "doi" in tool_config
    has_environment = "environment" in tool_config

    if isinstance(delegate, FormatDelegateV1_2):
        has_url = "location" in tool_config
        assert (
            has_url or has_doi or has_environment
        ), "The actual tool is missing (no URL or DOI of a tool archive and no Docker image)"
        assert not (
            has_url and has_doi
        ), "Two tool archives provided (one by a URL, one by a DOI), it is unclear which one should be used"

    elif isinstance(delegate, FormatDelegateV2_0):
        has_url = "url" in tool_config
        if not (has_url or has_doi or has_environment):
            raise CoVeriLangException(
                "The actual tool is missing (no URL or DOI of a tool archive and no Docker image)"
            )
        if has_url and has_doi:
            raise CoVeriLangException(
                "Two tool archives provided (one by a URL, one by a DOI), it is unclear which one should be used"
            )

    return has_url, has_doi


def load_policy(policy_file):
    if not policy_file:
        if os.getenv("COVERITEAM_POLICY"):
            policy_file = os.getenv("COVERITEAM_POLICY")
        else:
            return {}

    with open(policy_file, "r") as f:
        try:
            return yaml.safe_load(f)
        except yaml.YAMLError as e:
            msg = "Failed to load policy from: {} Error is: {}".format(policy_file, e)
            raise CoVeriLangException(msg, 202)


def update_cache_entry(cache_path: Path, key, value):
    with open(cache_path, "r") as json_cache_in:
        updated_dict = json.load(json_cache_in)
    updated_dict[key] = value
    with open(cache_path, "w") as json_cache_out:
        json.dump(updated_dict, json_cache_out)


def check_policy_compliance_allowed_locations(allowed_locations, archive_location):
    # Expression to match nothing.
    e = "a^"
    for loc in allowed_locations:
        e = "%s|(%s)" % (e, re.escape(loc))
    if not re.compile(e).match(archive_location):
        msg = "Not allowed to download from the url: %s" % archive_location
        raise CoVeriLangException(msg, 100)


def check_policy_compliance(ac, policy_file=None):
    policy = load_policy(policy_file)
    allowed_locations = policy.get("allowed_locations", None)
    if allowed_locations:
        check_policy_compliance_allowed_locations(
            allowed_locations, ac.archive_location
        )


def check_allowed_doi(doi):
    parts = doi.rsplit("/")
    if len(parts) != 2:
        raise CoVeriLangException("Strange DOI! This DOI is not allowed.")

    if parts[0] != "10.5281" or parts[1].rsplit(".")[0] != "zenodo":
        raise CoVeriLangException(
            "This DOI is not allowed. Only Zenodo DOIs are allowed."
        )


def check_missing_version_exception(name, versions):
    log_version_warnings(name)

    if len(versions) <= 0:
        raise CoVeriLangException(f"{name} doesn't have any versions specified!")


def log_version_warnings(actor_name):
    logging.warning(
        "No version specified for actor %s. Taking the first version from the actor definition file.",
        actor_name,
    )
    logging.warning(
        "You can specify the tool's version as a third parameter in the call to ActorFactory.create()."
    )


def get_archive_url_from_zenodo_doi(doi):
    ZENODO_API_URL_BASE = "https://zenodo.org/api/records/"
    zenodo_record_id = doi.rsplit(".")[-1]
    url = ZENODO_API_URL_BASE + zenodo_record_id
    response = SESSION_PROVIDER.get_session().get(url, timeout=60)
    data = json.loads(response.content)
    if len(data["files"]) > 1:
        raise CoVeriLangException(
            "More than one files are linked in this Zenodo record. The record should have only one file."
        )
    archive_url = data["files"][0]["links"]["self"]
    archive_checksum = data["files"][0]["checksum"]
    return archive_url, archive_checksum

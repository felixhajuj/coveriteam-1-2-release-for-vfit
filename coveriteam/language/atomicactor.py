# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
import argparse
import logging
import os
import shutil
import tempfile
import uuid
from enum import Enum
from pathlib import Path
from string import Template
from typing import Dict, Optional
from xml.etree import ElementTree

import benchexec.containerexecutor as containerexecutor
import docker
from benchexec import tooladapter
from benchexec.container import DIR_FULL_ACCESS, DIR_HIDDEN, DIR_OVERLAY, DIR_READ_ONLY
from benchexec.model import cmdline_for_run, load_tool_info
from benchexec.runexecutor import RunExecutor
from benchexec.test_benchmark_definition import DummyConfig

from coveriteam import util
from coveriteam.language import CoVeriLangException
from coveriteam.language.actor import Actor
from coveriteam.language.actorconfig import ActorConfig
from coveriteam.language.artifact import Artifact, AtomicActorDefinition, Program
from coveriteam.util import (
    TOOL_OUTPUT_FILE,
    get_additional_paths_for_container_config,
    get_TOOL_INFO_DOWNLOAD_PATH,
    str_dict,
)


class TaskExecutionInformation:
    # Note: Although the functionality of this class is dependent on the objects/classes from RunExec, we have decided
    # to keep it like this and create similar objects from the Docker based execution too. This allows us to seamlessly
    # use the tool-info modules.
    def __init__(self, command, exec_output, log_file, log_dir, is_runexec=True):
        self.command = command
        self.log_file = log_file
        self.log_dir = log_dir
        if is_runexec:
            self.runexec_execution = True
            self.run_object = self.__create_run_object_from_runexec_output(exec_output)
        else:
            self.runexec_execution = False
            self.run_object = self.__create_run_object_from_docker_output(exec_output)

    def __create_run_object_from_runexec_output(self, runexec_output):
        """
        Creates Run object to be used by the determine_result function from ToolInfo module
        """
        try:
            with open(self.log_file, "rt", errors="ignore") as outputFile:
                output = outputFile.readlines()
                # first 6 lines are for logging, rest is output of subprocess, see runexecutor.py for details
                output = output[6:]
        except IOError as e:
            logging.warning("Cannot read log file: %s", e.strerror)
            output = []

        exit_code = runexec_output.get("exitcode")
        output = tooladapter.CURRENT_BASETOOL.RunOutput(output)
        run = tooladapter.CURRENT_BASETOOL.Run(self.command, exit_code, output, None)
        return run

    def __create_run_object_from_docker_output(self, exec_output):
        """
        Creates Run object to be used by the determine_result function from ToolInfo module
        """
        exit_code, output_log = exec_output
        output = tooladapter.CURRENT_BASETOOL.RunOutput(output_log)
        run = tooladapter.CURRENT_BASETOOL.Run(self.command, exit_code, output, None)
        return run


class AtomicActor(Actor):
    def __init__(self, path, version=None, resource_limits=None):
        super().__init__()
        if isinstance(path, AtomicActorDefinition):
            path = path.path
        config = ActorConfig(path, version, resource_limits)

        # At the moment, environment is defined only when using docker container.
        # Might need to change it later.
        if config.environment:
            self.tool = DockerBasedTool(config)
        else:
            self.tool = ArchiveBasedTool(config)

    def name(self):
        return self.tool.name()

    def print_version(self):
        self.tool.print_version()
        self.tool.close()

    def act(self, **kwargs):
        if self.tool.is_stopping():
            raise RuntimeError(
                f"act() called in {self.name()}, but it's trying to stop"
            )
        self.tool.set_state_preparing()

        res = super().act(**kwargs)

        return res

    def gen_xml_elem(self, inputs, outputs, **kwargs):
        super().gen_xml_elem(inputs, outputs, **kwargs)
        self.xml_elem.append(self.tool.gen_measurements_xml())
        tool_output_elem = ElementTree.Element("tool_output")
        tool_output_elem.text = str(
            Actor._get_relative_path_to_actor(self.tool.log_file())
        )
        self.xml_elem.append(tool_output_elem)

    def _act(self, **kwargs):
        # TODO this is the function that is taking care of actual action. It's name should be changed.
        args = self._prepare_args(**kwargs)
        # arg substitution is only for the options defined in the actor definitions
        d = self._get_arg_substitutions(**kwargs)
        options = [Template(o).safe_substitute(**d) for o in self.tool.options()]
        task_options = get_task_metadata(kwargs.setdefault("program", None))
        try:
            execution_config = {
                "input_dirs": get_input_dirs(kwargs),
                "file_pattern_to_extract": self._result_files_patterns,
            }
            execution_output = self.tool.run_tool(
                *args,
                options,
                task_options=task_options,
                execution_config=execution_config,
            )
            res = self._extract_result(execution_output)
            self.tool.close()
            return res
        except UnboundLocalError:
            msg = "The execution of the actor {} did not produce the expected result\n".format(
                self.name()
            )
            msg += "More information can be found in the logfile produced by the tool: {}".format(
                self.tool.log_file()
            )
            raise CoVeriLangException(msg)
        finally:
            self.tool.set_state_idle()

    def _prepare_args(self, **kwargs):
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError

    def _get_arg_substitutions(self, **kwargs):
        # This is a placeholder method that can be overriden by subclasses.
        return {}

    def _extract_result(self, te_info: TaskExecutionInformation) -> Dict[str, Artifact]:
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError

    @property
    def execution_state(self):
        return self.tool._execution_state


def get_task_metadata(program: Optional[Program] = None) -> dict:
    """
    This function returns the task options to be passed to the tool info module
    to create a command.
    At the moment we only have two kinds of programs: C and Java.

    Args:
        program: A Program to analyze for metadata

    Returns:
        A dictionary which contains all necessary metadata for the given program.
        If no program or None is given, the dictionary is empty.
    """
    if not program:
        return {}
    d = {"language": program.get_language()}
    if program.data_model:
        d["data_model"] = program.data_model
    return d


def get_input_dirs(inputs):
    ds = set()
    for v in inputs.values():
        if not hasattr(v, "path"):
            continue
        if isinstance(v.path, str):
            ds.add(str(Path(v.path).parent.resolve()))
        elif isinstance(v.path, list):
            for path in v.path:
                ds.add(str(Path(path).parent.resolve()))
    return ds


class UnderlyingTool:
    def __init__(self, config):
        self._execution_state = ExecutionState.Idle
        self._config = config
        self._atomic_execution_id = None

        # initialized by the subclass
        self._tool = None

    def name(self):
        return self._config.actor_name

    def options(self):
        return self._config.options

    def log_dir(self):
        # actor execution id is for the complete execution of an actor -- atomic or composite
        # atomic execution id is for this specific atomic actor.
        return (
            Actor.get_top_actor_execution_dir()
            / self.name()
            / self._atomic_execution_id
        )

    def log_file(self):
        return self.log_dir() / TOOL_OUTPUT_FILE

    def get_relative_path_to_tool(self, path):
        return os.path.relpath(path, self._config.tool_dir) if path else ""

    def is_stopping(self):
        return self._execution_state is ExecutionState.Stopping

    def set_state_idle(self):
        self._execution_state = ExecutionState.Idle

    def set_state_preparing(self):
        self._execution_state = ExecutionState.Preparing

    def print_version(self):
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError

    def run_tool(
        self,
        program_path,
        property_path,
        additional_options=[],
        options=[],
        task_options={},
        execution_config=None,
    ):
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError

    def gen_measurements_xml(self):
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError

    def stop(self):
        if self._execution_state is ExecutionState.Executing:
            self._stop()
        elif self._execution_state is ExecutionState.Preparing:
            # Next execution will be skipped
            self._execution_state = ExecutionState.Stopping
        elif self._execution_state is ExecutionState.Stopping:
            logging.info("stop() called in %s, but it is already stopping", self.name())
        else:
            logging.info("stop() called in %s, ignored because of idling", self.name())

    def close(self):
        try:
            self._tool.close()
        except Exception:
            raise CoVeriLangException("Tool hasn't been initialized, so cannot close!")

    def _create_config_for_container_execution(self):
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError

    def _create_command(
        self, options, additional_options, program_path, property_path, task_options
    ):
        if program_path:
            identifier = None
            if isinstance(program_path, str):
                program_path = [self.get_relative_path_to_tool(program_path)]
            elif isinstance(program_path, list):
                program_path = [self.get_relative_path_to_tool(p) for p in program_path]
        else:
            identifier = "no-program-file"

        property_path = self.get_relative_path_to_tool(property_path)

        tool_name = self._config.tool_name or self.name()

        tool_info, self._tool = load_tool_info(
            tool_name, self._create_config_for_container_execution()
        )

        resource_limits = tooladapter.CURRENT_BASETOOL.ResourceLimits(
            self._config.reslim.get("timelimit"),
            self._config.reslim.get("timelimit"),
            None,
            self._config.reslim.get("memlimit"),
            self._config.reslim.get("cpuCores"),
        )
        tool_locator = tooladapter.CURRENT_BASETOOL.ToolLocator(
            use_path=True, use_current=True
        )
        tool_executable = self._tool.executable(tool_locator)

        # TODO This is bad. It has to change. cmd should not be a part of the actor.
        # But we need it to extract result since PR 592 in benchexec.
        cmd = cmdline_for_run(
            self._tool,
            tool_executable,
            options + additional_options,
            program_path,
            identifier,
            property_path,
            task_options,
            resource_limits,
        )
        return cmd, tool_executable

    def _stop(self):
        # This is a template method that must be overriden by subclasses.
        raise NotImplementedError


class ExecutionState(Enum):
    Idle = 1  # Idling, not planned to start at the moment
    Preparing = 2  # In setup phase, thus can not be stopped
    Executing = 3  # Currently operating, thus able to stop
    Stopping = 4  # Next scheduled execution will be skipped


class ArchiveBasedTool(UnderlyingTool):
    def __init__(self, config):
        super().__init__(config)
        self.__run_executor = None
        self.__measurements = {}
        self.__dir_modes = {}
        self.__initialize_directory_modes()

    def print_version(self):
        cwd = os.getcwd()
        os.chdir(self._config.tool_dir)

        tool_name = self._config.tool_name
        tool_info, self._tool = load_tool_info(
            tool_name, self._create_config_for_container_execution()
        )
        tool_locator = tooladapter.CURRENT_BASETOOL.ToolLocator(
            use_path=True, use_current=True
        )
        version = self._tool.version(self._tool.executable(tool_locator))
        print(self._tool.name() + " " + version)
        os.chdir(cwd)

    def run_tool(
        self,
        program_path,
        property_path,
        additional_options=[],
        options=[],
        task_options={},
        execution_config=None,
    ):
        assert all(
            k in execution_config for k in ("input_dirs", "file_pattern_to_extract")
        )

        # Generate atomic execution id
        self._atomic_execution_id = str(uuid.uuid4())

        self.__configure_input_directories(execution_config["input_dirs"])

        # Change directory to tool's directory
        cwd = os.getcwd()
        os.chdir(self._config.tool_dir)

        lims_for_exec = {
            "softtimelimit": self._config.reslim.get("timelimit"),
            "memlimit": self._config.reslim.get("memlimit"),
        }
        cmd, tool_executable = self._create_command(
            options, additional_options, program_path, property_path, task_options
        )

        # Test for stopping the execution of this atomic actor
        self.__run_executor = RunExecutor(dir_modes=self.__dir_modes)

        logging.debug("RunExecutor created for %s", self.name())

        # Run stopped before actual execution, skipping execution
        if self._execution_state is ExecutionState.Stopping:
            self.__measurements = get_emtpy_measurements()
            # State idle set later, after the processing of the measurements
            return TaskExecutionInformation(
                cmd,
                self.__measurements,
                logfile=self.log_file(),
                log_dir=self.log_dir(),
            )

        self._execution_state = ExecutionState.Executing

        self.__measurements = self.__run_executor.execute_run(
            cmd,
            str(self.log_file().resolve()),
            output_dir=str(self.log_dir().resolve()),
            result_files_patterns=execution_config["file_pattern_to_extract"],
            workingDir=self._tool.working_directory(tool_executable),
            environments=self._tool.environment(tool_executable),
            **lims_for_exec,
        )

        terminationreason = self.__measurements.get("terminationreason")
        if terminationreason:
            logging.warning(
                "The actor %s was terminated by BenchExec. Termination reason: %s."
                " Possibly it did not produce the expected result.",
                self.name(),
                terminationreason,
            )

        # Change back to the original directory
        os.chdir(cwd)

        return TaskExecutionInformation(
            cmd, self.__measurements, log_file=self.log_file(), log_dir=self.log_dir()
        )

    def gen_measurements_xml(self):
        data_filter = ["cputime", "walltime", "memory"]
        data = {
            k: self.__measurements[k] for k in data_filter if k in self.__measurements
        }
        return ElementTree.Element("measurements", str_dict(data))

    def _create_config_for_container_execution(self):
        if hasattr(Actor, "trust_tool_info") and Actor.trust_tool_info:
            return DummyConfig

        # We use the standard container config if the field doesn't exist.

        parser = argparse.ArgumentParser()
        containerexecutor.add_basic_container_args(parser)
        containerexecutor.add_container_output_args(parser)
        mp = {
            DIR_HIDDEN: "--hidden-dir",
            DIR_OVERLAY: "--overlay-dir",
            DIR_READ_ONLY: "--read-only-dir",
            DIR_FULL_ACCESS: "--full-access-dir",
        }
        args = []
        for p in get_additional_paths_for_container_config():
            args += ["--full-access-dir", p]
        for k, v in self.__dir_modes.items():
            args += [mp[v], k]
        config = parser.parse_args(args)
        config.container = True
        return config

    def _stop(self):
        self.__run_executor.stop()

    def __initialize_directory_modes(self):
        # The default directory modes taken from container executor.
        self.__dir_modes = {
            "/": DIR_READ_ONLY,
            "/run": DIR_HIDDEN,
            "/tmp": DIR_HIDDEN,  # noqa S108
            "/sys": DIR_HIDDEN,
            "/home": DIR_HIDDEN,
        }
        # Update the default with the /sys and /home as hidden.
        if Actor.allow_cgroup_access:
            self.__dir_modes["/sys/fs/cgroup"] = DIR_FULL_ACCESS

        self.__dir_modes[util.get_CACHE_DIR_PATH()] = DIR_READ_ONLY
        self.__dir_modes[self._config.tool_dir] = DIR_OVERLAY
        self.__dir_modes[str(get_TOOL_INFO_DOWNLOAD_PATH())] = DIR_OVERLAY

    def __configure_input_directories(self, input_dirs):
        for d in input_dirs:
            self.__dir_modes[d] = DIR_FULL_ACCESS


def get_emtpy_measurements() -> Dict:
    """
    Returns a dictionary, which contains the minimum measurement data as keys.
    """
    return {
        "walltime": 0,
        "cputime": 0,
        "memory": 0,
    }


class DockerBasedTool(UnderlyingTool):
    def __init__(self, config):
        super().__init__(config)
        self.__docker_image_repo = self._config.environment["image_repo"]
        self.__docker_image_tag = self._config.environment["image_tag"]
        self.__docker_image_uri = (
            f"{self.__docker_image_repo}:{self.__docker_image_tag}"
        )
        self.__container = None

    def print_version(self):
        tool_name = self._config.tool_name
        tool_info, self._tool = load_tool_info(
            tool_name, self._create_config_for_container_execution()
        )

        # We do not support getting the version from DockerBasedTools because the tool info module might try to execute
        # the tool in a BenchExec container to get the version, and we can not support that in a Docker container.
        print(self._tool.name() + "\n")
        print(
            "Printing the version is not supported for tools executed in a Docker environment."
        )

    def run_tool(
        self,
        program_path,
        property_path,
        additional_options=[],
        options=[],
        task_options={},
        execution_config=None,
    ):
        assert all(
            k in execution_config for k in ("input_dirs", "file_pattern_to_extract")
        )

        # Generate atomic execution id
        self._atomic_execution_id = str(uuid.uuid4())

        # Change directory to tool's directory
        cwd = os.getcwd()
        os.chdir(self._config.tool_dir)

        cmd = self._create_command(
            options, additional_options, program_path, property_path, task_options
        )[0]

        exec_output = self.__execute_in_docker(cmd, execution_config)
        # Change back to the original directory
        os.chdir(cwd)

        return TaskExecutionInformation(
            cmd, exec_output, self.log_file(), self.log_dir(), False
        )

    def gen_measurements_xml(self):
        measurements_xml = ElementTree.Element("measurements")
        comment_xml = ElementTree.Comment(
            "Resource measurements are not supported for actors created from Docker images"
        )
        measurements_xml.append(comment_xml)
        return measurements_xml

    def _create_config_for_container_execution(self):
        return DummyConfig

    def _stop(self):
        self.__container.stop()

    def __get_docker_client_and_pull_image(self):
        client = docker.from_env()
        print(
            f"Pulling the docker image for actor {self.name()} from {self.__docker_image_uri}"
        )
        client.images.pull(self.__docker_image_repo, tag=self.__docker_image_tag)
        print(f"Finished pulling the docker image for actor {self.name()}")
        return client

    def __execute_in_docker(self, cmd, execution_config):
        dirs = execution_config["input_dirs"]
        client = self.__get_docker_client_and_pull_image()
        working_dir = self._config.tool_dir
        dirs.add(working_dir)
        dirs = map(Path, dirs)
        temp_root = tempfile.mkdtemp(prefix="coveriteam_docker_")
        (
            volume_list,
            volume_mapping_for_container,
        ) = DockerBasedTool.create_overlay_volumes(client, dirs, temp_root)

        self.__container = client.containers.create(
            image=self.__docker_image_uri,
            command=cmd,
            volumes=volume_mapping_for_container,
            working_dir=working_dir,
            detach=True,
        )

        # Run stopped before actual execution, skipping execution
        if self._execution_state is ExecutionState.Stopping:
            self.__container.remove()
            for volume in volume_list:
                volume.remove()

            # State idle set later, after the processing of the measurements
            return (
                None,
                "Tool execution was skipped because the actor was shut down before the setup was complete",
            )

        self._execution_state = ExecutionState.Executing
        exit_code = None
        output = None
        try:
            self.__container.start()
            exit_code = self.__container.wait()["StatusCode"]
            output = self.__container.logs()
        except docker.errors.ContainerError as e:
            logging.error(
                "The command did not finish with exit code 0 in the container."
            )
            if e.__container.logs():
                output = e.__container.logs()
            else:
                output = e.stderr
        except Exception as e:
            logging.error("Unsuccessful Docker run")
            raise CoVeriLangException(e)
        finally:
            try:
                shutil.rmtree(temp_root)
            except PermissionError:
                logging.warning(
                    "Couldn't delete the temporary file due to permission issues. "
                    "Most probably Docker is running as a root. "
                    "This might fill the directory /tmp. "
                )
            # Remove the container and volumes
            self.__container.remove()
            for volume in volume_list:
                volume.remove()
            output = (
                output
                if output
                else bytes(
                    "Tool execution did not produce anything on stdout or stderr\n",
                    "utf-8",
                )
            )
            save_log(cmd, output, self.log_file())
            extract_result_files(
                os.path.join(temp_root, "upper"),
                self.log_dir(),
                execution_config["file_pattern_to_extract"],
            )
        return exit_code, output.decode("utf-8").split("\n")

    @staticmethod
    def create_overlay_volumes(client, dirs, temp_root):
        upper_dir_root = os.path.join(temp_root, "upper")
        os.mkdir(upper_dir_root)
        working_dir_root = os.path.join(temp_root, "working")
        os.mkdir(working_dir_root)

        volume_mapping_for_container = []
        volume_list = []
        for d in dirs:
            # Variables for overlay
            overlay_volume_name = str(uuid.uuid4())
            lower_dir = str(d.resolve())
            upper_dir = os.path.join(upper_dir_root, overlay_volume_name)
            os.mkdir(upper_dir)
            working_dir = os.path.join(working_dir_root, overlay_volume_name)
            os.mkdir(working_dir)

            overlay_options = (
                f"lowerdir={lower_dir},upperdir={upper_dir},workdir={working_dir}"
            )
            volume = client.volumes.create(
                name=overlay_volume_name,
                driver="local",
                driver_opts={
                    "device": "overlay",
                    "type": "overlay",
                    "o": overlay_options,
                },
            )

            volume_mapping_for_container.append(f"{volume.name}:{lower_dir}")
            volume_list.append(volume)

        return volume_list, volume_mapping_for_container


def save_log(cmd, log, log_file):
    log_file.parent.mkdir(exist_ok=True, parents=True)
    with log_file.open("wb") as f:
        # To keep the output log same as produced by RunExec
        f.write(" ".join(cmd).encode())
        f.write(
            "\n\n\n--------------------------------------------------------------------------------\n\n\n".encode()
        )
        f.write(log)


def extract_result_files(root_dir, logdir, file_patterns):
    files = []
    for pattern in file_patterns:
        files_found = Path(root_dir).glob(pattern)
        files += files_found if files_found else []

    for f in files:
        target = Path(logdir) / f.relative_to(root_dir)
        target.parent.mkdir(parents=True)
        shutil.copy(f, target)

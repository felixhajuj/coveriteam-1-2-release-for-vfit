# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.7.2

from io import StringIO
from pathlib import Path
from typing import Any, Dict

import yaml

from coveriteam.interpreter import get_parsed_tree
from coveriteam.interpreter.python_code_generator import (
    COVERITEAM_IMPORTS,
    CoVeriLangToPythonConverter,
)
from coveriteam.language.actorconfig import ActorConfig
from coveriteam.parser.CoVeriLangParser import CoVeriLangParser
from coveriteam.parser.CoVeriLangVisitor import CoVeriLangVisitor
from coveriteam.utils.json_validator import FlatMappingValidator


class AtomicActorVisitor(CoVeriLangVisitor):
    """Only resolves atomic actors.

    Creates ActorConfigs to download these actors.
    """

    def __init__(self, path):
        # I need the cvt path to resolve the file paths.
        self.__cvt_path = Path(path).parent.resolve()

        # Importing everything from CoVeriTeam
        exec(COVERITEAM_IMPORTS, globals())  # noqa S102
        super().__init__()

    def visitAtomic(self, ctx: CoVeriLangParser.AtomicContext):
        actordef = ctx.name.text
        if ctx.name.type == CoVeriLangParser.STRING:
            actordef = str((self.__cvt_path / actordef.strip('"')).resolve())

        version = ""
        if ctx.version:
            # Sanitizing the version from surrounding " and '
            version = ctx.version.text.strip("'").strip('"')

        # Download of the atomic actor
        ActorConfig(actordef, version)


def download_atomic_actors(path_str):
    tree = get_parsed_tree(path_str)
    AtomicActorVisitor(path_str).visit(tree)


def remove_quotes(s):
    return s[1:-1]


def requote(s):
    return '"' + s + '"'


# This class defines a complete listener for a parse tree produced by CoVeriLangParser.
class FileCollector(CoVeriLangVisitor):
    files = []

    def __init__(self, path):
        # TODO don't know a better way to do it.
        # I need the cvt path to resolve the file paths.
        self.__cvt_path = Path(path).parent.resolve()
        super().__init__()

    # Visit a parse tree produced by CoVeriLangParser#spec_stmt.
    def visitSpec_stmt(self, ctx: CoVeriLangParser.Spec_stmtContext):
        a = ctx.assignable()
        if a.STRING():
            self.files += [remove_quotes(a.STRING().getText())]
        else:
            self.visitAssignable(a)

    # Visit a parse tree produced by CoVeriLangParser#Atomic.
    def visitAtomic(self, ctx: CoVeriLangParser.AtomicContext):
        if ctx.STRING():
            actordef = remove_quotes(ctx.STRING().getText())
            self.files += [actordef]

    # Visit a parse tree produced by CoVeriLangParser#artifact.
    def visitArtifact(self, ctx: CoVeriLangParser.ArtifactContext):
        if ctx.STRING():
            path = remove_quotes(ctx.STRING().getText())
            self.files += [path]


def collect_files(path_str):
    tree = get_parsed_tree(path_str)

    visitor = FileCollector(path_str)
    visitor.visit(tree)
    return visitor.files


class FileRename(CoVeriLangVisitor):
    def __init__(self, path, substitutions: Dict[str, str]):
        # TODO don't know a better way to do it.
        # I need the cvt path to resolve the file paths.
        self.__cvt_path = Path(path).parent.resolve()
        self.substitutions = substitutions
        super().__init__()

    # Visit a parse tree produced by CoVeriLangParser#spec_stmt.
    def visitSpec_stmt(self, ctx: CoVeriLangParser.Spec_stmtContext):
        a = ctx.assignable()
        if a.STRING():
            file = remove_quotes(a.STRING().getText())
            replacement = self.substitutions.get(file, file)
            a.STRING().getSymbol().text = requote(replacement)
        else:
            self.visitAssignable(a)

    # Visit a parse tree produced by CoVeriLangParser#Atomic.
    def visitAtomic(self, ctx: CoVeriLangParser.AtomicContext):
        """
        The atomic actor context can contain up to three strings:

        ActorFactory.create(actordef, "path", "version", "resource_path")
                                      ^^^^^^   ^^^^^^^^   ^^^^^^^^^^^^^
        """
        if ctx.STRING():
            for s in ctx.STRING():
                string_literal = remove_quotes(s.getText())
                replacement = self.substitutions.get(string_literal, string_literal)
                s.getSymbol().text = requote(replacement)

    # Visit a parse tree produced by CoVeriLangParser#artifact.
    def visitArtifact(self, ctx: CoVeriLangParser.ArtifactContext):
        if ctx.STRING():
            path = remove_quotes(ctx.STRING().getText())
            replacement = self.substitutions.get(path, path)
            ctx.STRING().getSymbol().text = requote(replacement)


def rename_files(path_str, substitutions: Dict[str, str]):
    tree = get_parsed_tree(path_str)

    visitor = FileRename(path_str, substitutions)
    visitor.visit(tree)
    return tree


def get_rename_schema():
    # the schema allows only entries of type "name": "string"
    return {
        "type": "object",
        "patternProperties": {
            ".*": {"type": "string"},
        },
        "additionalProperties": False,
    }


class RenameSchemaValidationError(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)

    def __str__(self):
        return f"""
    RenameSchemaValidationError:

    The schema for the rename file could not be validated.
    The following errors occured:

    {self.message}
    """.strip()


def validate_rename_data(rename_data: Any) -> Dict[str, str]:
    """
    Validates a rename file against a schema.

    raises RenameSchemaValidationError if the schema is not valid.
    """

    validator = FlatMappingValidator(str, str)

    if not validator.is_valid(rename_data):
        error_msg = StringIO("Invalid rename file:\n")
        for error in validator.iter_errors(rename_data):
            print(error, file=error_msg, sep="\n")

        raise RenameSchemaValidationError(error_msg.getvalue())

    return rename_data


def generate_code_with_file_renaming(path_str, rename_path):
    with open(rename_path) as yaml_file:
        data = yaml.safe_load(yaml_file)

    substitutions = validate_rename_data(data)

    tree = rename_files(path_str, substitutions)
    visitor = CoVeriLangToPythonConverter(path_str)
    visitor.visit(tree)
    return visitor.pyp
